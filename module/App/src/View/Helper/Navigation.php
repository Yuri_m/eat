<?php

namespace App\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Navigation extends AbstractHelper
{
    
    /**
     *
     * @var object
     * 
     */
    protected $serviceLocator;
    
    /**
     *
     * @var object 
     * 
     */
    protected $model;
    
    
    public function __invoke()
    {
        $this->model = $this->serviceLocator->get(\App\Dashboard\Model\CategoryModel::class);
        return $this;
    }
    
    /**
     * 
     * @return array
     * 
     */
    public function getCategoriesList()
    {
        return $this->model->getCategories();
    }
    
    public function setServiceLocator($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }
    
    
}