<?php

namespace App\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Tag\Cloud;

class TagCloud extends AbstractHelper
{
    
    /**
     *
     * @var object
     * 
     */
    protected $serviceLocator;
    
    /**
     *
     * @var object 
     * 
     */
    protected $model;
    
    
    public function __invoke()
    {
        $this->model = $this->serviceLocator->get(\App\Dashboard\Model\CategoryModel::class);
        return $this;
    }
    
    public function setServiceLocator($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }
    
    /**
     * 
     * @return array
     */
    public function getTags()
    {
        // Create the cloud and assign static tags to it
        $cloud = new Cloud(array(
            'cloudDecorator' => array(
                'decorator' => 'htmlcloud',
                'options'   => array(
                    'separator' => "\n\n",
                    'htmlTags'  => array(
                        'ul' => array(
                            'class' => 'my_custom_class',
                            'id'    => 'tag-cloud',
                        ),
                    ),
                ),
            ),
            'tags' => array(
                array(
                    'title'  => 'Code',
                    'weight' => 50,
                    'params' => array('url' => '/tag/code'),
                ),
                array(
                    'title'  => 'Zend Framework',
                    'weight' => 1,
                    'params' => array('url' => '/tag/zend-framework'),
                ),
                array(
                    'title' => 'PHP',
                    'weight' => 5,
                    'params' => array('url' => '/tag/php'),
                ),
            ),
        ));

        // Render the cloud
        return $cloud;
    }
    
}