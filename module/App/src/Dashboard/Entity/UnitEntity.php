<?php

namespace App\Dashboard\Entity;

/**
 * Units entity.
 */
class UnitEntity
{
    
    /**
     *
     * @var int
     */
    protected $id;
  
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var float
     */
    protected $price;
    
    /**
     *
     * @var int
     */
    protected $productId;
    	
    /**
     *
     * @var int
     */
    protected $unitTypeId;
    
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getProductId() {
        return $this->productId;
    }

    public function getUnitTypeId() {
        return $this->unitTypeId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function setProductId($productId) {
        $this->productId = $productId;
    }

    public function setUnitTypeId($unitTypeId) {
        $this->unitTypeId = $unitTypeId;
    }


}