<?php

namespace App\Dashboard\Entity;

/**
 * Categories of items.
 */
class CategoryEntity
{
    
    /**
     *
     * @var int
     */
    protected $id;
  
    /**
     *
     * @var
     */
    protected $name;
    
    /**
     *
     * @var
     */
    protected $parentId;
  
    /**
     *
     * @var
     */
    protected $keyword;
    
    /**
     *
     * @var
     */
    protected $isActive;
    
    /**
     *
     * @var
     */
    protected $updatedAt;
    
    /**
     *
     * @var
     */
    protected $createdAt;
    
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getParentId() {
        return $this->parentId;
    }

    public function getKeyword() {
        return $this->keyword;
    }

    public function getIsActive() {
        return $this->isActive;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setParentId($parentId) {
        $this->parentId = $parentId;
    }

    public function setKeyword($keyword) {
        $this->keyword = $keyword;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }


}