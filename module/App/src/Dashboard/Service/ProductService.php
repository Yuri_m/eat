<?php

namespace App\Dashboard\Service;

//use App\Service\ServiceInterface;
use App\Dashboard\Service\ProductServiceInterface;
use App\Dashboard\Model\CategoryModelInterface;
use App\Dashboard\Model\ProductModelInterface;

class ProductService implements ProductServiceInterface
{
    
    protected $modelCategory;
    protected $modelProduct;
    
    public function __construct(CategoryModelInterface $modelCategory, ProductModelInterface $modelProduct)
    {
        $this->modelCategory = $modelCategory;
        $this->modelProduct = $modelProduct;
    }
    
    public function findProductsByCategoryAction($categoryId)
    {
        if (!$categoryId) {
            throw new \Exception('It is necessary to pass id.');
        }
        
        $result['category'] = $this->modelCategory->fetchActiveById($categoryId);
        
        if (!$result['category']) {
            throw new \Exception('An empty entity. No data in the database.');
        }
        
        $categoriesIds = $this->modelCategory->getParentChildrenIds($categoryId);
        
        $products = $this->modelProduct->fetchJoinByCategoryIds($categoriesIds);
        $result['productsSorted'] = $this->modelProduct->sortProductsById($products);
        
        return $result;
    }
}