<?php

namespace App\Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use App\Dashboard\Service\ProductServiceInterface;
 
class IndexController extends AbstractActionController
{

    protected $service;


    public function __construct(ProductServiceInterface $service)
    {
        $this->service = $service;
    }

    public function indexAction()
    {
        $view = new ViewModel();
        $view->setTemplate('dashboard/index/index');
        return $view;
    }
    
    public function productsByCategoryAction()
    {
        $categoryId = (int) $this->params()->fromRoute('id', 0);
        $result = $this->service->findProductsByCategoryAction($categoryId);
        
        $view = new ViewModel();
        $view->setTemplate('dashboard/index/products-by-category');
        $view->setVariables(array(
            'category'       => $result['category'],
            'productsSorted' => $result['productsSorted'],
        ));
        return $view;
    }
 
}