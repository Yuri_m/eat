<?php

namespace App\Dashboard\Table;

use Zend\Db\TableGateway\AbstractTableGateway;
use App\Dashboard\Table\CategoryTableInterface;

class CategoryTable extends AbstractTableGateway implements CategoryTableInterface
{
    
    const TABLE_NAME = 'categories';
    
    /**
     *
     * @var Zend\Db\TableGateway\AbstractTableGateway 
     * 
     */
    private $tableGateway;

    
    public function setTableGateway(AbstractTableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Fetch all of data
     * 
     * @return object
     */
    public function fetchAll()
    {
        return $this->tableGateway->select();
    }
    
    /**
     * Fetch active categories
     * 
     * @return object
     */
    public function fetchActive()
    {
        return $this->tableGateway->select(array('is_active' => '1'));
    }
    
    /**
     * Fetch active category by category id
     * 
     * @param int $id
     * @return object
     */
    public function fetchActiveById($id)
    {
        $data = $this->tableGateway->select(array(
            'id' => $id,
            'is_active' => '1'
        ));
        
        return $data->current();
    }
    

}