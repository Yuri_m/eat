<?php

namespace App\Dashboard\Table;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;

use App\Dashboard\Table\ProductTableInterface;

class ProductTable extends AbstractTableGateway implements ProductTableInterface
{
    
    const TABLE_NAME = 'products';
    
    /**
     *
     * @var Zend\Db\TableGateway\AbstractTableGateway 
     * 
     */
    private $tableGateway;

    
    public function setTableGateway(AbstractTableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Fetch active products by categories ids
     * with units and unit_types
     *
     * @param array $categoriesIds
     */
    public function fetchJoinByCategoryIds($categoriesIds)
    {
        $adapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = new \Zend\Db\Sql\Sql($adapter);

        $select = new Select() ; 
        $select->from(['p' => 'products']); 
        $select->join(
            ['c' => 'categories'],
            'p.category_id = c.id',
            [],
            Select::JOIN_LEFT
        );
        $select->join(
            ['u' => 'units'],
            'p.id = u.product_id',
            ['unit_name' => 'name', 'unit_price' => 'price'],
            Select::JOIN_LEFT
        );
        $select->join(
            ['ut' => 'unit_types'],
            'u.unit_type_id = ut.id',
            ['unit_types_name' => 'name'],
            Select::JOIN_LEFT
        );
        $select->where->in('p.category_id', $categoriesIds);
        $select->where(array('p.is_active' => '1'));
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        return $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
    }
    
}