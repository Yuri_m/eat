<?php

namespace App\Table;

use Zend\Db\TableGateway\AbstractTableGateway;

class UnitTable extends AbstractTableGateway
{
    
    const TABLE_NAME = 'units';
    
    /**
     *
     * @var Zend\Db\TableGateway\AbstractTableGateway 
     * 
     */
    protected $tableGateway;

    
    public function setTableGateway(AbstractTableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
 
    /**
     * Get units by products ids
     * 
     * @param array $productsIds
     * @return object
     */
    public function getUnitsByProductsIds($productsIds)
    {
        return $this->tableGateway->select(array(
            'product_id' => $productsIds
        ));
    }
    
}