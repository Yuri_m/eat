<?php

namespace App\Dashboard\Model;

use Zend\Stdlib\Hydrator\ClassMethods;

use App\Dashboard\Model\ProductModelInterface;
use App\Dashboard\Table\ProductTableInterface;
use App\Dashboard\Entity\ProductCategoryUnitEntity;

class ProductModel implements ProductModelInterface
{
    
    /**
     * 
     * @var
     */
    private $table;
    
    
    public function __construct(ProductTableInterface $table)
    {
        $this->table = $table;
    }
    
    /**
     * Fetch array of product entities
     * 
     * @param array $categoriesIds
     * @return array of App\Entity\ProductCategoryUnitEntity
     */
    public function fetchJoinByCategoryIds($categoriesIds)
    {
        $rows = $this->table->fetchJoinByCategoryIds($categoriesIds);

        $hydrator = new ClassMethods();
        $products = array();
        
        foreach ($rows as $row) {
            $products[] = $hydrator->hydrate((array)$row, new ProductCategoryUnitEntity());
        }
        
        return $products;
    }
    
    /**
     * Sort products entities by product id
     * 
     * @param array $entities Array of App\Entity\ProductCategoryUnitEntity
     * @return array
     * 
     */
    public function sortProductsById($entities)
    {
        $products = array();
        foreach ($entities as $entity) {
            $products[$entity->getId()][] = $entity;
        }
        
        return $products;
    }
}