<?php

namespace App\Model;

use App\Table\UnitTable;

class UnitModel
{
    
    /**
     * 
     * @var
     */
    private $table;
    
    
    public function setTable(UnitTable $table)
    {
        $this->table = $table;
    }
    
    /**
     * Get units by products ids
     * 
     * @param array $productsIds
     * @return object
     */
    public function getUnitsByProductsIds($productsIds)
    {
        return $this->table->getUnitsByProductsIds($productsIds);
    }
    
    
}