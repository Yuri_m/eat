<?php

namespace App\Dashboard\Model;

use App\Dashboard\Table\CategoryTableInterface;
use App\Dashboard\Model\CategoryModelInterface;

class CategoryModel implements CategoryModelInterface
{

    /**
     * 
     * @var
     */
    private $table;
    
    
    public function __construct(CategoryTableInterface $table)
    {
        $this->table = $table;
    }
    
    /**
     * Fetch active category by category id
     * 
     * @param int $id
     * @return object
     */
    public function fetchActiveById($id)
    {
        return $this->table->fetchActiveById($id);
    }
    
    /**
     * Get categories
     * 
     * @return array
     */
    public function getCategories()
    {
        $categories = $this->table->fetchActive()->toArray();
        $sortedCategories = $this->sortByParents($categories);
        return $this->buildCategoriesRecursion(0, $sortedCategories);
    }
    
    /**
     * Sort categories by ids and parent categories
     * 
     * @param array $categories
     * @return array
     */
    private function sortByParents($categories)
    {
        $sortedCategories = array(
            'categories' => array(),
            'parent_categories' => array()
        );
        
        foreach ($categories as $val) {
            $sortedCategories['categories'][$val['id']] = $val;
            $sortedCategories['parent_categories'][$val['parent_id']][] = $val['id'];
        }
        
        return $sortedCategories;
    }
    
    /**
     * Build array of subcategories
     * @param int $parent
     * @param array $category
     * @param array $children
     * @param int $level
     * @return array
     * 
     */
    private function buildCategoriesRecursion($parent, $category, $children = array(), $level = 0)
    {
        
        if (isset($category['parent_categories'][$parent])) {
            
            foreach ($category['parent_categories'][$parent] as $categoryId) {
                if (isset($category['parent_categories'][$categoryId])) {
                    $children = $this->buildCategoriesRecursion($categoryId, $category, $children, ++$level);
                    --$level;
                }
                
                $category['categories'][$categoryId]['level'] = $level;
                
                // Add current category at the beginning of the common array of categories
                array_unshift($children, $category['categories'][$categoryId]);
            }
        }
        
        return $children;
    }
    
    /**
     * Get ids of children categories by category id
     * 
     * @param int $id
     * @return array
     */
    public function getChildrenIds($id)
    {
        $categories = $this->table->fetchActive()->toArray();
        $sortedCategories = $this->sortByParents($categories);
        return $this->findChildrenCategoriesRecursion($id, $sortedCategories);
    }
    
    /**
     * Get ids of children categories by parent id
     * with own parent id
     * 
     * @param int $id
     * @return array
     */
    public function getParentChildrenIds($id)
    {
        $ids = $this->getChildrenIds($id);
        $ids[] = $id;
        return $ids;
    }
    
    /**
     * Get children of categories
     * 
     * @return array
     * 
     */
    private function findChildrenCategoriesRecursion($parent, $category)
    {
        $children = array();
        
        if (isset($category['parent_categories'][$parent])) {
            
            foreach ($category['parent_categories'][$parent] as $categoryId) {
                if (isset($category['parent_categories'][$categoryId])) {
                    $children = $this->findChildrenCategoriesRecursion($categoryId, $category);
                }
                
                $children[] = (int)$categoryId;
            }
        }
        
        return $children;
    }
    
}