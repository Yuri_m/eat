<?php

namespace App\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ClassMethods;

use App\Table\UnitTable;

class UnitTableFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new HydratingResultSet(new ClassMethods(), new \App\Entity\UnitEntity());
        $tableGateway = new TableGateway(UnitTable::TABLE_NAME, $dbAdapter, null, $resultSetPrototype);
        $table = new UnitTable();
        $table->setTableGateway($tableGateway);
        return $table;
    }
}