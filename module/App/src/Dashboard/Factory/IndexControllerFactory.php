<?php

namespace App\Dashboard\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $controller = new \App\Dashboard\Controller\IndexController($serviceLocator->getServiceLocator());
        return $controller;
    }
}