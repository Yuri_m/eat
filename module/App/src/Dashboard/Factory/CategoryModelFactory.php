<?php

namespace App\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use App\Model\CategoryModel;

class CategoryModelFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $table = $serviceLocator->get(\App\Table\CategoryTable::class);
        $model = new CategoryModel();
        $model->setTable($table);
        return $model;
    }
}