<?php

namespace App\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ViewHelperTagCloudFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $helper = new \App\View\Helper\TagCloud();
        $helper->setServiceLocator($serviceLocator->getServiceLocator());
        return $helper;
    }
}