<?php

namespace App\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ViewHelperNavigationFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $helper = new \App\View\Helper\Navigation();
        $helper->setServiceLocator($serviceLocator->getServiceLocator());
        return $helper;
    }
}