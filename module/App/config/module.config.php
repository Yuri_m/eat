<?php

return array(
    'di' => [
        'allowed_controllers' => [
            App\Dashboard\Controller\IndexController::class,
        ],
        'instance' => [
            'preference' => [
                App\Dashboard\Service\ProductServiceInterface::class => App\Dashboard\Service\ProductService::class,
                App\Dashboard\Model\CategoryModelInterface::class => App\Dashboard\Model\CategoryModel::class,
                App\Dashboard\Model\ProductModelInterface::class => App\Dashboard\Model\ProductModel::class,
                App\Dashboard\Table\CategoryTableInterface::class => App\Dashboard\Table\CategoryTable::class,
                App\Dashboard\Table\ProductTableInterface::class => App\Dashboard\Table\ProductTable::class,
            ],
        ],
    ],

    'service_manager' => array(
        'factories' => array(
            App\Dashboard\Table\CategoryTable::class => App\Dashboard\Factory\CategoryTableFactory::class,
            App\Dashboard\Table\ProductTable::class => App\Dashboard\Factory\ProductTableFactory::class,
        ),
//        'invokables' => array(
//        ),
    ),
    
    'view_helpers' => array(
        'factories' => array(
            'appNavigation' => App\Factory\ViewHelperNavigationFactory::class,
            'tagCloud'      => App\Factory\ViewHelperTagCloudFactory::class,
        ),
    ),
    
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => require __DIR__ . '/../template_map.php',
    ),
    
    'module_layouts' => array(
        'App' => 'app/layout',
    ),
    
);