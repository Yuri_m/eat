<?php

return array(
    'router' => array(
        'routes' => array(
            'app' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => App\Dashboard\Controller\IndexController::class,
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'products_by_category' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => 'category/[:id/]',
                            'constraints' => array(
                                'id'     => '[0-9]+',
                            ),
                            'defaults' => array(
                                'action' => 'productsByCategory',
                            ),
                        ),
                    ),
                ),    // <<< child_routes
            ),
        ),
    ),
);