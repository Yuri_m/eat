<?php

return array(
    'controllers' => array(
        'factories' => array(
            Admin\Controller\Admin::class => Admin\Factory\AdminControllerFactory::class,
            Admin\Controller\Category::class => Admin\Factory\CategoryControllerFactory::class,
            Admin\Controller\Product::class => Admin\Factory\ProductControllerFactory::class,
        ),
    ),

    'service_manager' => array(
        'factories' => array(
            Admin\Model\CategoryModel::class => Admin\Factory\CategoryModelFactory::class,
            Admin\Table\CategoryTable::class => Admin\Factory\CategoryTableFactory::class,
            Admin\Form\CategoryAddForm::class => Admin\Factory\CategoryAddFormFactory::class,
            Admin\Form\CategoryEditForm::class => Admin\Factory\CategoryEditFormFactory::class,
            Admin\Model\ProductModel::class => Admin\Factory\ProductModelFactory::class,
            Admin\Table\ProductTable::class => Admin\Factory\ProductTableFactory::class,
        ),
        'invokables' => array(
            Admin\Entity\CategoryEntity::class => Admin\Entity\CategoryEntity::class,
            Admin\Entity\ProductEntity::class => Admin\Entity\ProductEntity::class,
            Admin\Entity\ProductCategoryEntity::class => Admin\Entity\ProductCategoryEntity::class,
        ),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            'admin/layout'            => __DIR__ . '/../view/layout/layout.phtml',
        ),
    ),
    
    'module_layouts' => array(
        'Admin' => 'admin/layout',
    ),
    
);