<?php

namespace Admin;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\EventManager\EventInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;

class Module implements ConfigProviderInterface
{
    
    public function onBootstrap(EventInterface $event)
    {
        $moduleRouteListener = new ModuleRouteListener();
        $eventManager = $event->getApplication()->getEventManager();
        $sharedManager = $eventManager->getSharedManager();
        $moduleRouteListener->attach($eventManager);

        $sharedManager->attach(
            'Zend\Mvc\Controller\AbstractActionController',
            MvcEvent::EVENT_DISPATCH,
            [$this, 'initLayout'],
            100
        );
    }
    
    public function initLayout(MvcEvent $event)
    {
        $controller = $event->getTarget();
        $controllerClass = get_class($controller);
        $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));

        $config = $event->getApplication()->getServiceManager()->get('config');
        
        if (isset($config['module_layouts'][$moduleNamespace])) {
            $controller->layout($config['module_layouts'][$moduleNamespace]);
        }
    }
    
    public function getConfig()
    {
        $config = array_merge(
            include 'config/module.config.php',
            include 'config/router.config.php'
        );
        
        return $config;
    }
}