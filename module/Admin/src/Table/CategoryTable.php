<?php

namespace Admin\Table;

use Zend\Db\TableGateway\AbstractTableGateway;

class CategoryTable extends AbstractTableGateway
{
    
    const TABLE_NAME = 'categories';
    
    /**
     *
     * @var Zend\Db\TableGateway\AbstractTableGateway 
     * 
     */
    protected $tableGateway;

    
    public function setTableGateway(AbstractTableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Fetch all of data
     * 
     * @return
     */
    public function fetchAll()
    {
        return $this->tableGateway->select();
    }
    
    /**
     * Insert data DB
     * @param array $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->tableGateway->insert($data);
    }
    
    /**
     * Update data DB
     * @param array $data
     * @return mixed
     */
    public function edit($data)
    {
        return $this->tableGateway->update($data, array('id' => $data['id']));
    }
    
    /**
     * Fetch data by id
     * @param string/int $id
     * @return object
     */
    public function fetchById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

}