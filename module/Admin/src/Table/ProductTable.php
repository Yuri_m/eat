<?php

namespace Admin\Table;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;

class ProductTable extends AbstractTableGateway
{
    
    const TABLE_NAME = 'products';
    
    /**
     *
     * @var Zend\Db\TableGateway\AbstractTableGateway 
     * 
     */
    protected $tableGateway;

    
    public function setTableGateway(AbstractTableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Fetch all of data
     * 
     * @return
     */
    public function fetchAll()
    {
        return $this->tableGateway->select();
    }
    
    /**
     * Fetch all of data with category name
     * 
     *
     */
    public function fetchJoinCategory()
    {
        $adapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = new \Zend\Db\Sql\Sql($adapter);

        $select = new Select() ; 
        $select->from(['p' => 'products']); 
        $select->join(
            ['c' => 'categories'],
            'p.category_id = c.id',
            ['category_name' => 'name'],
            Select::JOIN_LEFT
        );
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        return $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
    }
    
}