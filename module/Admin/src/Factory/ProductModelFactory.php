<?php

namespace Admin\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Admin\Model\ProductModel;

class ProductModelFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $table = $serviceLocator->get(\Admin\Table\ProductTable::class);
        $model = new ProductModel();
        $model->setTable($table);
        return $model;
    }
}