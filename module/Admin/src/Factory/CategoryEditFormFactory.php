<?php

namespace Admin\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Admin\Form\CategoryEditForm;

class CategoryEditFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $model = $serviceLocator->get(\Admin\Model\CategoryModel::class);
        $form = new CategoryEditForm();
        $form->setModel($model);
        $form->init();
        return $form;
    }
}