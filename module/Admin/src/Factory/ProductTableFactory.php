<?php

namespace Admin\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ClassMethods;

use Admin\Table\ProductTable;

class ProductTableFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new HydratingResultSet(new ClassMethods(), new \Admin\Entity\ProductEntity());
        $tableGateway = new TableGateway(ProductTable::TABLE_NAME, $dbAdapter, null, $resultSetPrototype);
        $table = new ProductTable();
        $table->setTableGateway($tableGateway);
        return $table;
    }
}