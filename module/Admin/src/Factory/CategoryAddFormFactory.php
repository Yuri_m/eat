<?php

namespace Admin\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Admin\Form\CategoryAddForm;

class CategoryAddFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $model = $serviceLocator->get(\Admin\Model\CategoryModel::class);
        $form = new CategoryAddForm();
        $form->setModel($model);
        $form->init();
        return $form;
    }
}