<?php

namespace Admin\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ClassMethods;

use Admin\Table\CategoryTable;

class CategoryTableFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new HydratingResultSet(new ClassMethods(), new \Admin\Entity\CategoryEntity());
        $tableGateway = new TableGateway(CategoryTable::TABLE_NAME, $dbAdapter, null, $resultSetPrototype);
        $table = new CategoryTable();
        $table->setTableGateway($tableGateway);
        return $table;
    }
}