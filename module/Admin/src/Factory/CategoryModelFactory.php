<?php

namespace Admin\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Admin\Model\CategoryModel;

class CategoryModelFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $table = $serviceLocator->get(\Admin\Table\CategoryTable::class);
        $model = new CategoryModel();
        $model->setTable($table);
        return $model;
    }
}