<?php

namespace Admin\Entity;

/**
 * Products entity.
 */
class ProductEntity
{
    
    /**
     *
     * @var int
     */
    protected $id;
  
    /**
     *
     * @var
     */
    protected $name;
    
    /**
     *
     * @var
     */
    protected $description;
    
    /**
     *
     * @var
     */
    protected $altUnique;
    
    /**
     *
     * @var
     */
    protected $isActive;
    
    /**
     *
     * @var
     */
    protected $categoryId;
    
    /**
     *
     * @var
     */
    protected $updatedAt;
    
    /**
     *
     * @var
     */
    protected $createdAt;
    
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getAltUnique() {
        return $this->altUnique;
    }

    public function getIsActive() {
        return $this->isActive;
    }

    public function getCategoryId() {
        return $this->categoryId;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setAltUnique($altUnique) {
        $this->altUnique = $altUnique;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    public function setCategoryId($categoryId) {
        $this->categoryId = $categoryId;
    }

    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }


}