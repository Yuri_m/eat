<?php

namespace Admin\Model;

use Zend\Stdlib\Hydrator\ClassMethods;

use Admin\Table\ProductTable;
use Admin\Entity\ProductCategoryEntity;

class ProductModel
{
    
    /**
     * 
     * @var
     */
    private $table;
    
    
    public function setTable(ProductTable $table)
    {
        $this->table = $table;
    }
    
    /**
     * Fetch data from DB
     * 
     * @return array
     */
    public function fetchAll()
    {
        return $this->table->fetchAll();
    }
    
    /**
     * Fetch all of data with category name
     * 
     * 
     */
    public function fetchJoinCategory()
    {
        $rows = $this->table->fetchJoinCategory();
        $hydrator = new ClassMethods();
        $entityCollection = array();
        
        foreach ($rows as $row) {
            $entityCollection[] = $hydrator->hydrate((array)$row, new ProductCategoryEntity());
        }
        
        return $entityCollection;
    }
    
}