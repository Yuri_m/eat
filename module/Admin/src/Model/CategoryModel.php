<?php

namespace Admin\Model;

use Zend\Stdlib\Hydrator\ClassMethods;
use Admin\Table\CategoryTable;
use Admin\Entity\CategoryEntity;

class CategoryModel
{
    
    /**
     * 
     * @var
     */
    private $table;
    
    
    public function setTable(CategoryTable $table)
    {
        $this->table = $table;
    }
    
    /**
     * Fetch data from DB
     * 
     * @return array
     */
    public function fetchAll()
    {
        return $this->table->fetchAll();
    }
    
    /**
     * 
     * @return array
     * 
     */
    public function getSubcategories()
    {
        $categories = $this->fetchAll()->toArray();

        foreach ($categories as $category) {
            $categoriesSortByParent[$category['parent_id']][$category['id']] = ucfirst($category['name']);
        }

        $result = $categoriesSortByParent[0];
        $categoriesSortByParent = array_slice($categoriesSortByParent, 0, null, true);
        
        $current = array();
        foreach ($categoriesSortByParent as $keyparent => $parent) {
            if (array_key_exists($keyparent, $result)) {
                foreach ($parent as $key => $category) {
                    $current[$key] = $result[$keyparent] . ' > ' . $category;
                }
            
                $result = $result + $current;
            }
        }
        
        asort($result);
        return $result;
    }
    
    /**
     * 
     * @param Admin\Entity\CategoryEntity $entity
     * @return mixed
     * 
     */
    public function create(CategoryEntity $entity)
    {
        $entity->setCreatedAt(date('Y-m-d H:i:s'));
        $entity->setUpdatedAt(date('Y-m-d H:i:s'));
        
        $hydrator = new ClassMethods();
        $data = $hydrator->extract($entity);
        
        return $this->table->create($data);
    }
    
    /**
     * 
     * @param Admin\Entity\CategoryEntity $entity
     * @return mixed
     * 
     */
    public function edit(CategoryEntity $entity)
    {
        $entity->setUpdatedAt(date('Y-m-d H:i:s'));
        $hydrator = new ClassMethods();
        $data = $hydrator->extract($entity);
        
        return $this->table->edit($data);
    }
    
    /**
     * Fetch data by id
     * @param string/int $id
     * @return Admin\Entity\CategoryEntity
     */
    public function fetchById($id)
    {
        return $this->table->fetchById($id);
    }
    
    /**
     *
     * @param string/int $id
     * @return array
     */
    public function getArrayData($id)
    {
        $result = $this->table->fetchById($id)->toArray();
        
        if (!empty($result)) {
            $result = $result[0];
        }
        
        return $result;
    }
}