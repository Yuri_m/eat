<?php

namespace Admin\Controller;

use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CategoryController extends AbstractActionController
{
    protected $serviceLocator;
    
    protected $model;
    
    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->model = $this->serviceLocator->get(\Admin\Model\CategoryModel::class);
    }

    public function indexAction()
    {
        $data = $this->model->fetchAll();

        $view = new ViewModel();
        $view->setVariables(array(
            'categories' => $data,
        ));
        return $view;
    }
    
    public function addAction()
    {
        $form = $this->serviceLocator->get(\Admin\Form\CategoryAddForm::class);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $entity = $form->getObject();
                $this->model->create($entity);
                return $this->redirect()->toRoute('admin/category');
            }
        }
        
        $view = new ViewModel();
        $view->setVariables(array(
            'form' => $form,
        ));
        
        return $view;
    }
    
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        
        if (!$id) {
            throw new Exception('It is necessary to pass id.');
        }
        
        $data = $this->model->getArrayData($id);
        
        if (empty($data)) {
            throw new Exception('An empty entity. No data in the database.');
        }
        
        $form = $this->serviceLocator->get(\Admin\Form\CategoryEditForm::class);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $entity = $form->getObject();
                $this->model->edit($entity);
                return $this->redirect()->toRoute('admin/category');
            }
            
        } else {
            $form->setData($data);
        }
        
        $view = new ViewModel();
        $view->setVariables(array(
            'form' => $form,
        ));
        
        return $view;
    }
 
}