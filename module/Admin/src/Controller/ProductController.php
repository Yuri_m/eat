<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
 
class ProductController extends AbstractActionController
{
    protected $serviceLocator;
    
    protected $model;
    
    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->model = $this->serviceLocator->get(\Admin\Model\ProductModel::class);
    }

    public function indexAction()
    {
        $data = $this->model->fetchJoinCategory();
        
        $view = new ViewModel();
        $view->setVariables(array(
            'products' => $data,
        ));
        
        return $view;
    }
    
}