<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
 
class AdminController extends AbstractActionController
{
    protected $serviceLocator;
    
    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function indexAction()
    {
//        $this->layout('admin/layout');
        
        $view = new ViewModel();
//        $view->setTemplate('user/index/index');
        $view->setVariables(array(
            'admin' => 'admin name',
        ));
        return $view;
    }
 
}