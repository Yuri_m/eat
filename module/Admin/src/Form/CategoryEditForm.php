<?php

namespace Admin\Form;

//use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
//use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;
use Admin\Entity\CategoryEntity;

/**
 * @package CourseInt\Form
 */
class CategoryEditForm extends Form implements InputFilterProviderInterface
{
    
    /**
     *
     * @var type Admin\Model\CategoryModel
     */
    private $model;
    
    /**
     * Init form
     */
    public function init()
    {
        $this->setAttribute('method', 'post')
            ->setHydrator(new ClassMethodsHydrator())
//            ->setInputFilter(new InputFilter())
            ->bind(new CategoryEntity());
         
        
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
            'options' => [
                'label' => 'ID'
            ]
        ]);
        
        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Category Name'
            ]
        ]);
        
        $this->add([
            'name' => 'parent_id',
            'type' => 'select',
            'options' => [
                'label' => 'Subcategory of',
                'value_options' => $this->getSubcategories(),
            ]
        ]);
        
        $this->add([
            'name' => 'is_active',
            'type' => 'checkbox',
            'options' => [
                'label' => 'Is Category Active',
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0,
            ],
            'attributes' => [
                'value' => 1,
            ]
        ]);
        
        $this->add([
            'name' => 'created_at',
            'type' => 'hidden',
        ]);
        
        $this->add([
            'name' => 'send',
            'attributes' => [
                'type' => 'submit',
                'value' => 'edit',
                'id' => 'editbutton'
            ]
        ]);
    }
    
    /**
     * 
     * @return array
     */
    public function getSubcategories()
    {
        return $this->model->getSubcategories();
    }
    
    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'id' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
                'filters' => [
                    [
                        'name' => 'StringTrim'
                    ]
                ]
            ],
            
            'name' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'max' => 100,
                        ],
                        'name' => 'Alnum',
                        'options' => [
                            'allowWhiteSpace' => true,
                        ],
                    ],
                ],
                'filters' => [
                    [
                        'name' => 'StringTrim'
                    ]
                ]
            ],
            'parent_id' => [
                'required' => true,
            ],
            'created_at' => [
                'required' => true,
            ],
        ];
    }
    
    public function setModel($model)
    {
        $this->model = $model;
    }
    
    public function getModel()
    {
        return $this->model;
    }
     
}