<?php

namespace Album\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AlbumControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $controller = new \Album\Controller\AlbumController($serviceLocator->getServiceLocator());
        return $controller;
    }
}