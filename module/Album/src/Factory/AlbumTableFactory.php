<?php

namespace Album\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ClassMethods;

use Album\Table\AlbumTable;

class AlbumTableFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new HydratingResultSet(new ClassMethods(), new \Album\Entity\AlbumEntity());
        $tableGateway = new TableGateway(AlbumTable::TABLE_NAME, $dbAdapter, null, $resultSetPrototype);
        $table = new AlbumTable();
        $table->setTableGateway($tableGateway);
        return $table;
    }
}