<?php

namespace Album\Entity;
  
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface; 
  
/**
 * A music album.
 *
 * @property string $artist
 * @property string $title
 * @property int $id
 */
class AlbumEntity
{
    
    /**
     *
     * @var
     */
    protected $id;
  
    /**
     *
     * @var
     */
    protected $artist;
  
    /**
     *
     * @var
     */
    protected $title;
  
    /**
     * 
     * @return
     *
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return
     *
     */
    public function getArtist() {
        return $this->artist;
    }

    /**
     * 
     * @return
     *
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * 
     * @param type $id
     * 
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @param type $artist
     * 
     */
    public function setArtist($artist) {
        $this->artist = $artist;
    }

    /**
     * 
     * @param type $title
     * 
     */
    public function setTitle($title) {
        $this->title = $title;
    }


}