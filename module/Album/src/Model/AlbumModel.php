<?php

namespace Album\Model;

use Album\Table\AlbumTable;

class AlbumModel
{
    
    /**
     * 
     * @var
     */
    private $table;
    
    
    public function setTable(AlbumTable $table)
    {
        $this->table = $table;
    }
    
    /**
     * Fetch data from DB
     * 
     * @return array
     */
    public function fetchAll()
    {
        return $this->table->getAlbum();
    }
    
}