<?php

return array(
    'controllers' => array(
        'factories' => array(
            'Album\Controller\Album' => 'Album\Factory\AlbumControllerFactory',
        ),
    ),

    'service_manager' => array(
        'factories' => array(
            'AlbumModel' => 'Album\Factory\AlbumModelFactory',
            'AlbumTable' => 'Album\Factory\AlbumTableFactory',
        ),
//        'invokables' => array(
//            'AlbumEntity' => 'Album\Entity\AlbumEntity',
//        ),
    ),
    
    'router' => array(
        'routes' => array(
            'album' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/album[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Album\Controller\Album',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
//            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
//            'error/404'               => __DIR__ . '/../view/error/404.phtml',
//            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    
    'module_layouts' => array(
        'Album' => 'layout/layout',
    ),
    
);